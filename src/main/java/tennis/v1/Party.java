package tennis.v1;

import static tennis.v1.players.Player.hasMaxScoreOrThisOneHas;
import static tennis.v1.players.Player.hasSameScoreThan;
import static tennis.v1.players.Player.isOneOfThemHasAdvantage;
import static tennis.v1.players.Player.theAdvantagedOne;
import tennis.v1.players.Player;
import tennis.v1.referee.Decision;
import tennis.v1.referee.Outcome;
import tennis.v1.referee.Referee;

public class Party {
    private final Player first;
    private final Player second;
    private final Referee referee;

    public Party(Player first, Player second) {
        this.first = first;
        this.second = second;
        this.referee = new Referee();
    }

    public Outcome outcome() {
        if (hasSameScoreThan(first, second)) {
            return referee.outcome(first, Decision.EQUALITY);
        }
        if (hasMaxScoreOrThisOneHas(first, second)) {
            Player advantaged = theAdvantagedOne(first, second);
            if (isOneOfThemHasAdvantage(first, second)) {
                return referee.outcome(advantaged, Decision.ADVANTAGED);
            }
            return referee.outcome(advantaged, Decision.WINNER);
        }
        return referee.outcome(first, Decision.SCORES)
            .with(referee.outcome(second, Decision.SCORES));
    }

    public void givePointTo(String playerName) {
        if (first.hasName(playerName)) {
            first.makesAPoint();
        } else {
            second.makesAPoint();
        }
    }
}
