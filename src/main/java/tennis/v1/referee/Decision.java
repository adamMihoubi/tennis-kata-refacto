package tennis.v1.referee;

public enum Decision {
    ADVANTAGED("Advantage"),
    WINNER("Win for"),
    EQUALITY(""),
    SCORES("");

    private final String value;

    Decision(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
