package tennis.v1.referee;

public class Outcome {
    private final String value;

    public Outcome(String value) {
        this.value = value;
    }

    public Outcome with(Outcome outcome) {
        return new Outcome(String.join("-", value, outcome.value));
    }

    @Override
    public String toString() {
        return value;
    }
}
