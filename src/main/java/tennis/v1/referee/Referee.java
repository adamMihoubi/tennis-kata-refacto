package tennis.v1.referee;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.Function;
import tennis.v1.players.Player;

public class Referee {
    private final Map<Decision, Function<Player, Outcome>> decisions;

    public Referee() {
        decisions = new EnumMap<>(Decision.class);
        decisions.put(Decision.EQUALITY, player -> new Outcome(player.equality()));
        decisions.put(Decision.ADVANTAGED,
            player -> new Outcome(player.declare(Decision.ADVANTAGED)));
        decisions.put(Decision.WINNER, player -> new Outcome(player.declare(Decision.WINNER)));
        decisions.put(Decision.SCORES, player -> new Outcome(player.score()));
    }

    public Outcome outcome(Player player, Decision decision) {
        return decisions.get(decision).apply(player);
    }
}
