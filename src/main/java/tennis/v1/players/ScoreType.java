package tennis.v1.players;

import java.util.Arrays;

enum ScoreType {
    LOVE("Love", 0),
    FIFTEEN("Fifteen", 1),
    THIRTY("Thirty", 2),
    FORTY("Forty", 3);

    private final String name;
    private final Integer value;

    ScoreType(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    static String nameOf(Integer value) {
        return Arrays.stream(values())
            .filter(type -> type.value.equals(value))
            .findFirst()
            .map(type -> type.name)
            .orElseThrow(() -> new IllegalArgumentException("incorrect Value for score type"));
    }
}
