package tennis.v1.players;

import tennis.v1.EqualityTypes;
import tennis.v1.referee.Decision;

public class Player {
    private static final String SPACE = " ";
    private final Score score;
    private final String name;

    public Player(String name) {
        this.name = name;
        score = new Score();
    }

    public static Player theAdvantagedOne(Player first, Player second) {
        return first.score.greaterThan(second.score) ? first : second;
    }

    public static boolean isOneOfThemHasAdvantage(Player first, Player second) {
        return Math.abs(first.score.substract(second.score)) == 1;
    }

    public static boolean hasMaxScoreOrThisOneHas(Player first, Player second) {
        return first.hasMaxScore() || second.hasMaxScore();
    }

    public static boolean hasSameScoreThan(Player first, Player second) {
        return first.score.equals(second.score);
    }

    public boolean hasName(String name) {
        return this.name.equals(name);
    }

    public void makesAPoint() {
        score.increment();
    }

    public String equality() {
        return EqualityTypes.valueOf(score.value());
    }

    public String score() {
        return score.scoreType();
    }

    public String declare(Decision decision) {
        return String.join(SPACE, decision.getValue(), name);
    }

    private boolean hasMaxScore() {
        return score.max();
    }
}
