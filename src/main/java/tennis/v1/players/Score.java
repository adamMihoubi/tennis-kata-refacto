package tennis.v1.players;

import java.util.Objects;

public class Score {
    private int value;

    public void increment() {
        value++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Score score = (Score) o;
        return value == score.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    int value() {
        return value;
    }

    boolean greaterThan(Score score) {
        return this.value > score.value;
    }

    boolean max() {
        return this.value > 3;
    }

    int substract(Score score) {
        return this.value - score.value;
    }

    String scoreType(){
        return ScoreType.nameOf(value);
    }
}
