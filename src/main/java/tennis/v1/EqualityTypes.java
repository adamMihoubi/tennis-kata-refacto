package tennis.v1;

import java.util.Arrays;

public enum EqualityTypes {
    LOVE_ALL(0, "Love-All"),
    FIFTEEN_ALL(1, "Fifteen-All"),
    THIRTY_ALL(2, "Thirty-All"),
    DEUCE("Deuce");

    private final Integer score;
    private final String name;

    EqualityTypes(Integer score, String name) {
        this.score = score;
        this.name = name;
    }

    EqualityTypes(String name) {
        score = null;
        this.name = name;
    }

    public static String valueOf(Integer score) {
        return Arrays.stream(values())
            .filter(value -> score.equals(value.score))
            .findFirst()
            .map(equalityTypes -> equalityTypes.name)
            .orElse(DEUCE.name);
    }
}
