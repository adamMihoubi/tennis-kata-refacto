package tennis.v1;

import tennis.TennisGame;
import tennis.v1.players.Player;

public class TennisGame1 implements TennisGame {
    private final Party party;

    public TennisGame1(String player1Name, String player2Name) {
        party = new Party
            (
                new Player(player1Name),
                new Player(player2Name)
            );
    }

    public void wonPoint(String playerName) {
        party.givePointTo(playerName);
    }

    public String getScore() {
        return party.outcome()
            .toString();
    }
}
